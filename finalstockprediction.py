import numpy as np
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense, Dropout
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns
import yfinance as yf
import datetime as date
import streamlit as st
from fbprophet.plot import plot_plotly
from prophet import Prophet
from plotly import graph_objs as go
from keras.models import load_model
import datetime



start = '2010-01-01'
end = date.datetime.now()#'2019-12-01'
#df = yf.download('AAPL',start,end)

st.title("STOCK PREDICTION APPLICATION")
stocks = ("AAPL", "GOOG", "FB", "SBIN.BO")

selected_stock = st.selectbox("Select Dataset for Prediction : ", stocks)

#n_years = st.slider("Years of prediction : ", 1, 12)
#periods = n_years * 30


@st.cache
def load_data(ticker):
    df = yf.download(ticker, start, end)
    df.reset_index(inplace=True)
    return df


data_load_state = st.text("Loading data please wait.....")
df = load_data(selected_stock)
data_load_state.text("Loading Done....!!!")


st.subheader("Raw Data")
st.write(df.tail(10))


def plot_raw_data():
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df["Date"], y=df["Open"], name="Stock_Open"))
    fig.add_trace(go.Scatter(x=df["Date"], y=df["Close"], name="Stock_Close"))
    fig.layout.update(title_text="Time Series Data", xaxis_rangeslider_visible=True)
    st.plotly_chart(fig)


plot_raw_data()

st.subheader('Close Price VS Time Chart')
fig = plt.figure(figsize=(12, 6))
plt.plot(df.Close)
st.pyplot(fig)


st.subheader('Close Price VS Time Chart with MA 100 and 200')
ma100 = df.Close.rolling(100).mean()
ma200 = df.Close.rolling(200).mean()
fig = plt.figure(figsize=(12, 6))
plt.plot(ma100, 'r')
plt.plot(ma200, 'g')
plt.plot(df.Close)
st.pyplot(fig)

train_dates = pd.to_datetime(df['Date'])

#Variables for training
cols = list(df)[1:6]

df_for_training = df[cols[0]].astype(float)
df_for_training = pd.DataFrame(df_for_training)


sc = MinMaxScaler(feature_range = (0, 1))
df_for_training_scaled = sc.fit_transform(df_for_training)



trainX = []
trainY = []


n_future = 0   # Number of days we want to predict into the future
n_past = 20     # Number of past days we want to use to predict the future



for i in range(n_past, len(df_for_training_scaled) - n_future +1):
    trainX.append(df_for_training_scaled[i - n_past:i, 0:df_for_training.shape[1]])
    trainY.append(df_for_training_scaled[i + n_future - 1:i + n_future, 0])



trainX, trainY = np.array(trainX), np.array(trainY)

model = load_model('final_model.h5')


# to calulate diff between dates
input_date = st.sidebar.date_input('select date', date.datetime(2011,1,1))

diff = input_date-date.datetime.now()

#st.write(input_date)




n_future=60+5
                                        # 2019-10-12             60
forecast_period_dates = pd.date_range(list(train_dates)[-59], periods=n_future, freq='B').tolist()

forecast = model.predict(trainX[-n_future:]) #forecast 


forecast_copies = np.repeat(forecast, df_for_training.shape[1], axis=-1)
y_pred_future = sc.inverse_transform(forecast_copies)[:,0]


# Convert timestamp to date
forecast_dates = []
for time_i in forecast_period_dates:
    forecast_dates.append(time_i.date())
    

df_forecast = pd.DataFrame({'Date':np.array(forecast_dates), 'Open':y_pred_future})
df_forecast['Date']=pd.to_datetime(df_forecast['Date'])
df_forecast

original = df[['Date', 'Open']]
original['Date']=pd.to_datetime(original['Date'])
#original = original.loc[original['Date'] >= '2010-11-29']
original




st.subheader('Prediction VS Original')

def plot_data():
    fig2 = go.Figure()
    fig2.add_trace(go.Scatter(x=original['Date'], y=original["Open"], name="Stock_Open"))
    fig2.add_trace(go.Scatter(x=df_forecast["Date"], y=df_forecast["Open"], name="Stock_Close"))
    fig2.layout.update(title_text="Time Series Data", xaxis_rangeslider_visible=True)
    st.plotly_chart(fig2)


plot_data()



sns.lineplot(original['Date'], original['Open'])
sns.lineplot(df_forecast['Date'], df_forecast['Open'])















