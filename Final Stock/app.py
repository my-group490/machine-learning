import numpy as np
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense, Dropout
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns
import yfinance as yf
import datetime as date
import streamlit as st
from fbprophet.plot import plot_plotly
from prophet import Prophet
from plotly import graph_objs as go

start = '2010-01-01'
end = date.datetime.now()#'2019-12-01'
df = yf.download('AAPL',start,end)
df = df.reset_index()

st.title("STOCK PREDICTION APPLICATION")
stocks = ("AAPL", "GOOG", "FB", "SBIN.BO")

selected_stock = st.selectbox("Select Dataset for Prediction : ", stocks)

#n_years = st.slider("Years of prediction : ", 1, 12)
#periods = n_years * 30


@st.cache
def load_data(ticker):
    df = yf.download(ticker, start, end)
    df.reset_index(inplace=True)
    return df


data_load_state = st.text("Loading data please wait.....")
df = load_data(selected_stock)
data_load_state.text("Loading Done....!!!")

st.subheader("Raw Data")
st.write(df.head(5))

st.subheader('Close Price VS Time Chart')
fig = plt.figure(figsize=(12, 6))
plt.plot(df.Close)
st.pyplot(fig)

st.subheader('Close Price VS Time Chart with MA 100 and 200')
ma100 = df.Close.rolling(100).mean()
ma200 = df.Close.rolling(200).mean()
fig = plt.figure(figsize=(12, 6))
plt.plot(ma100, 'r')
plt.plot(ma200, 'g')
plt.plot(df.Close)
st.pyplot(fig)

# Spliting Traning and Testing data

data_training = pd.DataFrame(df['Close'][0:int(len(df) * 0.70)])
data_testing = pd.DataFrame(df['Close'][int(len(df) * 0.70):int(len(df))])

scalar = MinMaxScaler(feature_range=(0, 1))
data_training_array1 = scalar.fit_transform(data_training)

model = load_model('mymodl.h5')

past_100_days = data_training.tail(100)
final_df = past_100_days.append(data_testing, ignore_index=True)
input_data = scalar.fit_transform(final_df)

x_test = []
y_test = []

for i in range(100, input_data.shape[0]):
    x_test.append(input_data[i - 100:i])
    y_test.append(input_data[i, 0])

x_test, y_test = np.array(x_test), np.array(y_test)

y_predicted = model.predict(x_test)

scalar = scalar.scale_

scalar_factor = 1 / scalar[0]
y_predicted = y_predicted * scalar_factor
y_test = y_test * scalar_factor

st.subheader('Prediction VS Original')

fig2 = plt.figure(figsize=(12, 6))
plt.plot(y_test, 'b', label="Origial Price")
plt.plot(y_predicted, 'r', label="Predicted Price")
plt.xlabel('Date')
plt.ylabel('Price')
plt.legend()
st.pyplot(fig2)


st.subheader(" Data")
st.write(y_test)
st.subheader(" Data")
st.write(y_predicted)
