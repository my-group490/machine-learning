import streamlit as st
import datetime as date
import yfinance as yf
from fbprophet.plot import plot_plotly
from prophet import Prophet
from plotly import graph_objs as go

start = "2000-01-01"
end = "2021-05-01"#date.datetime.now()

st.title("STOCK PREDICTION APPLICATION")

stocks = ("AAPL", "GOOG", "FB","SBIN.BO")

selected_stock = st.selectbox("Select Dataset for Prediction : ", stocks)

n_years = st.slider("Years of prediction : ", 1, 12)
periods = n_years * 30


@st.cache
def load_data(ticker):
    data = yf.download(ticker, start, end)
    data.reset_index(inplace=True)
    return data


data_load_state = st.text("Loading data please wait.....")
data = load_data(selected_stock)
data_load_state.text("Loading Done....!!!")

st.subheader("Raw Data")
st.write(data.tail(10))


def plot_raw_data():
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=data["Date"], y=data["Open"], name="Stock_Open"))
    fig.add_trace(go.Scatter(x=data["Date"], y=data["Close"], name="Stock_Close"))
    fig.layout.update(title_text="Time Series Data", xaxis_rangeslider_visible=True)
    st.plotly_chart(fig)


plot_raw_data()

df_train = data[['Date', 'Close']]
df_train = df_train.rename(columns={"Date": "ds", "Close": "y"})

m = Prophet(daily_seasonality=True)
m.fit(df_train)

future = m.make_future_dataframe(periods=periods)
forcast = m.predict(future)

st.subheader("Forcast data")
st.write(data.tail(10))


st.write("Forcast Data")
fig1 = plot_plotly(m,forcast)
st.plotly_chart(fig1)


st.write("Forcast Components")
fig2 =m.plot_components(forcast)
st.write(fig2)
